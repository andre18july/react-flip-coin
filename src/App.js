import React from 'react';
import FlipCoin from './Components/FlipCoin/FlipCoin';
import './App.css';

function App() {
  return (
    <div className="App">
      <FlipCoin />
    </div>
  );
}

export default App;
