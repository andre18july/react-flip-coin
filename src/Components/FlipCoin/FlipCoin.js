import React, { Component } from 'react';
import Coin from '../Coin/Coin';

class FlipCoin extends Component {



    constructor(props){
        super(props);

        this.state = {
            face: "https://upload.wikimedia.org/wikipedia/commons/c/cd/S_Half_Dollar_Obverse_2016.jpg",
            countHead: 1,
            countTail: 0
        };

        this.flip = this.flip.bind(this);
    }


    flip(){
        const coin = [
            "https://upload.wikimedia.org/wikipedia/commons/c/cd/S_Half_Dollar_Obverse_2016.jpg",
            "http://www.pcgscoinfacts.com/UserImages/71009269r.jpg"
        ];

        const randNumber = Math.floor(Math.random() * 2);

        this.setState({
            face: coin[randNumber]
        });

        if(randNumber === 0){
            this.setState(prevState => { return { countHead: prevState.countHead + 1 } });
        }else if(randNumber === 1){
            this.setState(prevState => { return { countTail: prevState.countTail + 1 } });
        }

    }



    render(){

 
        return(
            <div>
                <h1>Flip the coin</h1>
                <p>
                    <div>Heads Count: {this.state.countHead}</div>
                    <div>Tails Count: {this.state.countTail}</div>
                    <div>Number of throwns: {this.state.countHead + this.state.countTail}</div>
                </p>
                <Coin face={this.state.face}/>
                <button onClick={this.flip}>Flip Coin</button>
            </div>
        )
    }
}


export default FlipCoin;

